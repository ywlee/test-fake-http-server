name := """test-fake-http-server"""

version := "1.0"

scalaVersion := "2.11.11"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.1",
  "org.eclipse.jetty" % "jetty-server" % "9.4.6.v20170531",
  "com.typesafe.play" %% "play-ahc-ws-standalone" % "1.1.3",
  "com.typesafe.play" %% "play-ws-standalone-json" % "1.1.3",
  "com.typesafe.akka" %% "akka-actor" % "2.5.3",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)


Keys.fork in Test := true
javaOptions in Test += "-Dconfig.file=src/main/resources/application.test.conf"