package com.example

import org.scalatest._
import org.eclipse.jetty.server._
import com.typesafe.config.ConfigFactory

class SenderSpec extends FlatSpec
  with Matchers
  with BeforeAndAfter
  with BeforeAndAfterAll {
  
  val address = ConfigFactory.load.getString("setting.mockServer.address")
  val port = ConfigFactory.load.getInt("setting.mockServer.port")
  val mockServer = new Server(new java.net.InetSocketAddress(address, port))

  val handler = new Handler
  mockServer.setHandler(handler)

  override def beforeAll {
    mockServer.start
  }

  override def afterAll {
    mockServer.stop
  }

  before {
    handler.postBody.clear
  }

  "Sender" should "send a post message" in {
    val sender = new Sender
    sender.send("abc")

    assert(handler.postBody(0) == "message=abc")
  }

  it should "send another message" in {
    val sender = new Sender
    sender.send("def")

    assert(handler.postBody(0) == "message=def")
  }
}
