package com.example

import javax.servlet.http._
import java.util.stream.Collectors
import scala.collection.mutable.ListBuffer
import org.eclipse.jetty.server._
import org.eclipse.jetty.server.handler.AbstractHandler

class Handler extends AbstractHandler {
  val postBody = ListBuffer[String]()

  def handle(
    target: String,
    baseRequest: Request,
    request: HttpServletRequest,
    response: HttpServletResponse) = {

    if ("POST".equalsIgnoreCase(request.getMethod())) {
      postBody += request.getReader.lines.collect(Collectors.joining(System.lineSeparator))
    }

    response.setContentType("text/htmlcharset=utf-8")
    response.setStatus(HttpServletResponse.SC_OK)
    baseRequest.setHandled(true)
  }
}
