package com.example

object Hello {
  def main(args: Array[String]): Unit = {
    val sender = new Sender
    sender.send("hello")
  }
}
