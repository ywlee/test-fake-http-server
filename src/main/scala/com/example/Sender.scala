package com.example

import play.api.libs.ws._
import play.api.libs.ws.ahc._
import play.api.libs.ws.DefaultBodyWritables._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits._
import com.typesafe.config.ConfigFactory

class Sender {
  def send(message: String) = {
    implicit val system = akka.actor.ActorSystem()
    implicit val materializer = akka.stream.ActorMaterializer()

    val wsClient = StandaloneAhcWSClient()
    try
    {
      val address = ConfigFactory.load.getString("setting.address")
      val timeout = ConfigFactory.load.getInt("setting.timeout")
      val postBody = "message=" + message
      Await.result(
        wsClient.url(address)
          .addHttpHeaders(("Content-Type", "application/x-www-form-urlencoded"))
          .post(postBody)
          .map { response =>
            val statusText: String = response.statusText
            val body = response.body.toString
            println(s"Got a response $statusText")
            println(body)
      }, timeout.seconds)
    } catch {
      case ex: Throwable => println("Exception: " + ex)
    } finally {
      wsClient.close()
      system.terminate()
    }
  }
}
